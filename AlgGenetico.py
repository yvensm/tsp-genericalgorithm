import Mapa
import CromossomoControl
import operator
import random


def criarPopulacaoInicial(populacao,popuTam,tamMapa):
    for i in range(popuTam):
        populacao.append(CromossomoControl.gerarCromossomoAleatorio(tamMapa))


def selecionarElite(eliteTam,populacao):
    populacaoRanked = rankElite(populacao)
    newGen = []
    i = 0
    for key in populacaoRanked:
        newGen.append(populacao[key[0]])
        i+=1
        if i==eliteTam:
            break
    return newGen


def rankElite(populacao):
    rank = {}
    for i in range(len(populacao)):
        rank[i]=CromossomoControl.funcaoObjetivo(populacao[i])
    rank = sorted(rank.items(),key=operator.itemgetter(1))
    return rank

def crossover(numFilhos,elite):
    filhos = []
    while len(filhos) < numFilhos:
        index1 = random.randint(0,len(elite)-1)
        index2 = random.randint(0,len(elite)-1)
        pCross = random.randint(0,len(elite[0])-1)
        filho1 = CromossomoControl.crossover(elite[index1],elite[index2],pCross)
        filho2 = CromossomoControl.crossover(elite[index2],elite[index1],pCross)
        filhos.append(filho1)
        if(len(filhos)<numFilhos):
            filhos.append(filho2)
    return filhos


def mutacao(populacao,mutProb):
    for i in populacao:
        CromossomoControl.mutarCromossomo(i,mutProb)


def getBestSolution(populacao):
    best = populacao[0]

    for i in populacao:
        if CromossomoControl.funcaoObjetivo(i) < CromossomoControl.funcaoObjetivo(best):
            best = i

    return best


tamMapa=4
popuTam=10
eliteTam=int(0.3*tamMapa)
elite=[]
populacao=[]
mutProb=0.01
numInteracao=1000

Mapa.gerarMapa(tamMapa)
criarPopulacaoInicial(populacao,popuTam,tamMapa)

it= 0
while it < numInteracao:
    populacao = selecionarElite(eliteTam,populacao)
    for i in crossover(tamMapa-eliteTam,populacao):
        populacao.append(i)
    mutacao(populacao,mutProb)
    newBest = getBestSolution(populacao)
    if it == 0:
        best = getBestSolution(populacao)
    elif CromossomoControl.funcaoObjetivo(newBest) < CromossomoControl.funcaoObjetivo(best):
        best = newBest

    it += 1
print(best ," Valor:", CromossomoControl.funcaoObjetivo(best))