import random
import Mapa

def printCromo(cromo):
    for i in cromo:
        print(i,"->",end='')

    print("")


def mutarCromossomo(cromo,mutProb):
    for i in range(len(cromo)-1):
        mut = random.random()
        if mut<=mutProb:
            indexChange = random.randint(0,len(cromo)-2)
            aux = cromo[i]
            cromo[i] = cromo[indexChange]
            cromo[indexChange] = aux
    cromo[len(cromo)-1] = cromo[0]

def gerarCromossomoAleatorio(cromoSize):
    cromo = []
    for i in range(cromoSize):
        cromo.append(i)

    x = random.randint(0,100)
    for i in range(x):
        random.shuffle(cromo)

    cromo.append(cromo[0])

    return cromo

def crossover(pai1, pai2,pCross):
    filho = []
    cromoSize = len(pai1)
    indexF = 0
    valor = 0
    indexP = 0
    for indexP in range(pCross):
        filho.append(pai1[indexP])
        indexF+=1

    indexP+=1
    while indexF < cromoSize-1:
        valor = pai2[indexP % (cromoSize-1)]
        if valor not in filho:
            filho.append(valor)
            indexF+=1
        indexP+=1

    filho.append(filho[0])
    return filho

def funcaoObjetivo(cromo):
    sum=0
    for i in range(len(cromo) - 1):
        sum+= Mapa.getDistancia(cromo[i],cromo[i+1])
    return sum


