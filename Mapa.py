import random
import math

mapa = []


def gerarMapa(tamMapa):
    for i in range(tamMapa):
        equal = False
        while True:
            x = random.randint(0,99)
            y = random.randint(0,99)
            for cidade in mapa:
                if cidade[0] == x and cidade[1] == y:
                    equal = True
            if not equal:
                break
        mapa.append([])
        mapa[i].append(x)
        mapa[i].append(y)
    return mapa


def getDistancia(a,b):
    return math.sqrt(((mapa[a][0] - mapa[b][0])**2) + ((mapa[a][1]-mapa[b][1])**2))



